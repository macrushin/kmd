package factory;

import graph.Graph;

public class GraphMaker {
    static public Graph getGraph(int countEdges, int countNodes, int minWeight, int maxWeight) {
        int[][] matrixIncidence = new int[countNodes][countEdges];
        for (int i = 0; i < countEdges; i++) {
            int nodePrev = (int) (Math.random() * countNodes); // генерация числа от 0 до countNodes - 1
            int nodeNext;
            do {
                nodeNext = (int) (Math.random() * countNodes);
            } while (nodeNext != nodePrev);
            int randomWeight = (int) (Math.random() * (maxWeight - minWeight + 1) + minWeight);// генерация рандомного веса [minWeight, maxWeight] (целые числа)
            matrixIncidence[nodePrev][i] = randomWeight;
            matrixIncidence[nodeNext][i] = -randomWeight;
        }
        return new Graph(matrixIncidence);
    }
}
